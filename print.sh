#!/bin/bash

set -ex

# input params
PRINTER_NAME=EPSON_XP-315
INPUT_FILES=~/Desktop/to_print/*.pdf  # ~/storage/downloads/to_print/*.pdf
REMOTE_HOST=print_gens_vpn

set +x

# create load folder if not existent
mkdir -p $(dirname "$INPUT_FILES")

# ask confirmation and send files to print via ssh
echo "Found files:"
ls $INPUT_FILES | xargs -i echo "-" {}

read -p "Print them all? " -n 1 -r
echo

if [[ $REPLY == [Yy] ]]
then
    for input_file in ${INPUT_FILES}; do
	cat "$input_file" | \
	    ssh "$REMOTE_HOST" "/usr/bin/lp -d ${PRINTER_NAME}";
	rm "$input_file"
    done
fi
