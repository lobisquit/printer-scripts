#!/bin/sh

set -ex

REMOTE_HOST=print_gens_vpn
SCANNER_DEVICE="epson2:net:192.168.1.99"
OUT_FOLDER=~/Desktop # ~/storage/downloads

set +x

ssh "$REMOTE_HOST" \
    "scanimage --device-name $SCANNER_DEVICE --mode Gray --resolution 300 --format jpeg --progress" \
    > "$OUT_FOLDER"/$(date -Iseconds).jpeg
